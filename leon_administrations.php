<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Suprresion des tables des plugins-dist retirés de SPIP 4
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 */
function leon_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	// Première installation
	$maj['create'] = array(
		array('sql_drop_table', 'spip_messages'),
		array('sql_alter', 'TABLE spip_auteurs DROP messagerie'),
		array('sql_drop_table', 'spip_breves'),
		array('sql_drop_table', 'spip_petitions'),
		array('sql_drop_table', 'spip_signatures'),
		array('sql_drop_table', 'spip_ortho_cache'),
		array('sql_drop_table', 'spip_ortho_dico'),
	);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Désinstallation du plugin
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 **/
function leon_vider_tables($nom_meta_base_version) {
	effacer_meta($nom_meta_base_version);
}